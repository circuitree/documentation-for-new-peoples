# DOCUMENTATION FOR NEW PEOPLES


Welcome to this documentation that will hastely attempt to go over all of our code.

## First things first:

Note:
* These documents are written in [Markdown](https://daringfireball.net/projects/markdown/)  
* If you want to write in markdown or understand the raw version, see [this cheat
  sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* This is intended to give you as much information as possible

## Actually important things

### Looking for Help:

Do not give up. Look for answers online.

* Google (or any other search engine) is going to be your best friend.
* I lied in that last line: [stackoverflow.com](stackoverflow.com) is your best
  friend, but google will take you to the relevant stackoverflow page.
* Make sure to actually read the answers on stackoverflow
* If you are really stuck, go to [github.com](github.com) and see if you can
  find relevant code there
* If you get errors in your code, and you do not know the problem, try copy pasting the error into a search engine.
* Remeber that programing is hard and will be frustrating at times. 
* If you are _really_ stuck, you could try asking questions on a forum somewhere.
* you can copy code from any other FRC or FTC team; for other codebases, you need to make sure it is licensed under the MIT License or BSD license before copy pasting

###### Good Forums:

* [stackoverflow](stackoverflow.com) **The* forum coders go to for help with programming
* [StackExchange](https://stackexchange.com/) Parent site of stackoverflow, has many other useful forums
*

* FRC's documentation is terrible and provides very little detail! Even the author gets horribly confused by it.

##### Getting started with your Development

